package uk.co.coolalien.alieninvaders;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

public class HorizontalNumberPicker extends RelativeLayout {

	private final EditText number;
	private final Button plusButton;
	private final Button minusButton;
	private int value = 0;


	public HorizontalNumberPicker(Context context, AttributeSet attrs) {
		super(context, attrs);

		LayoutInflater layoutInflater = (LayoutInflater)context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.horizontal_number_picker, this);

		OnClickListener onClickListener = new OnClickListener() {
			public void onClick(View v) {
				number.clearFocus();
				if (v.getId() == R.id.pickerPlus) {
					changeValueByOne(true);
				} else {
					changeValueByOne(false);
				}
			}
		};


		plusButton = (Button) findViewById(R.id.pickerPlus);
		plusButton.setOnClickListener(onClickListener);
		minusButton = (Button) findViewById(R.id.pickerMinus);
		minusButton.setOnClickListener(onClickListener);

		number = (EditText) findViewById(R.id.pickerText);
		number.setRawInputType(InputType.TYPE_CLASS_NUMBER);


	}

	private void changeValueByOne(boolean increment) {
		if (increment) {
			value+=1;
		} else {
			value-=1;
		}
		value = Math.max(value, 0);
		value = Math.min(value, 100);
		number.setText(String.valueOf(value));
		invalidate();
	}
	
	public int getValue(){
		return value;
	}
	
	public void setValue(int value){
		this.value = value;
		number.setText(String.valueOf(value));
		invalidate();
	}

}
